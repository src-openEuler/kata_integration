%global debug_package %{nil}
%define VERSION v1.0.0
%define RELEASE 11

Name:           kata-integration
Version:        %{VERSION}
Release:        %{RELEASE}
Summary:        Kata Container integration
License:        Apache 2.0
URL:            https://gitee.com/openeuler/kata_integration
Source0:        https://gitee.com/openeuler/kata_integration/repository/archive/v1.0.0?format=tar.gz#/%{name}-%{version}.tar.gz

BuildRoot:      %_topdir/BUILDROOT
BuildRequires: automake gcc glibc-devel glibc-static patch 

%description
This is a usefult tool for building Kata Container components.

%prep
%setup -q -c -a 0 -n %{name}-%{version}

%build

%clean

%files

%doc

%changelog
* Thu Jun 20 2024 zhangbowei<zhangbowei@kylinos.cn> - 1.0.0-11
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: modify kernel config for supporting shared_fs:virtio-fs option

* Mon Aug 28 2023 Vanient<xiadanni1@huawei.com> - 1.0.0-10
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:delete unused kata-micro-kernel.spec

* Wed Aug 18 2022 chengzeruizhi<chengzeruizhi@huawei.com> - 1.0.0-9
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:adapt to kata-containers

* Fri Aug 5 2022 xiadanni<xiadanni1@huawei.com> - 1.0.0-8
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:bump version to 1.0.0-8

* Sat Sep 5 2020 jiangpengf<jiangpengfei9@huawei.com> - 1.0.0-2
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:change Source format to URL

* Wed Aug 26 2020 jiangpengf<jiangpengfei9@huawei.com> - 1.0.0-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:add initial kata-integration.spec
